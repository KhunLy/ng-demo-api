export interface User {
    id: number;
    email: string;
    lastName: string;
    firstName: string;
    birthDate: string;
    roleId: number;
    roleName: string;
    name: string;
}
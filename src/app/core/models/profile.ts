export interface Profile {
    id: number;
    lastName: string;
    firstName: string;
    birthDate: Date;
}
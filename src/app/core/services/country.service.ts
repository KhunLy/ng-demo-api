import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private readonly url:string = 'https://restcountries.com/v3';

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll() : Observable<Country[]> {
    return this.httpClient.get<Country[]>(this.url + '/all');
  }

}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '../models/profile';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly url: string = 'http://khun.somee.com/api/user';

  constructor(
    private httpClient: HttpClient
  ) { }

  public update(p: Profile) : Observable<void>{
    return this.httpClient.put<void>(this.url, p);
  }
}

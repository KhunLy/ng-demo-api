import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from '../models/login';
import { map } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly TOKEN_KEY = 'TOKEN'; 
  private readonly url: string = 'http://khun.somee.com/api/security';

  public token!: string | null;

  public get user(): User | null {
    if(!this.token) return null;
    return jwt_decode<User>(this.token); 
  }

  constructor(private httpClient: HttpClient) { 
    this.token = localStorage.getItem(this.TOKEN_KEY);
  }

  login(data: Login) : Observable<void> {
    return this.httpClient.post<string>(this.url + '/login', data).pipe(map(token => {
      localStorage.setItem(this.TOKEN_KEY, token);
      this.token = token;
    }));
  }

  refreshToken() : Observable<void> {
    return this.httpClient.get<string>(this.url + '/refreshToken').pipe(map(token => {
      console.log(token)
      localStorage.setItem(this.TOKEN_KEY, token);
      this.token = token;
    }))
  }

  logout() {
    localStorage.removeItem(this.TOKEN_KEY);
    this.token = null;
  }
}

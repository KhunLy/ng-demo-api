import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokedex } from '../models/pokedex';
import { PokemonDetails } from '../models/pokemon-details';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  private readonly apiUrl = 'https://pokeapi.co/api/v2/pokemon';

  constructor(
    private httpClient: HttpClient
  ) { }

  public get(limit: number, offset: number) : Observable<Pokedex> {
    let params = new HttpParams();
    params = params.append('limit', limit);
    params = params.append('offset', offset);
    // permet d'effectuer une requete en passant des paramatres de requete
    return this.httpClient.get<Pokedex>(this.apiUrl, { params })
  }

  public getDetails(name: string) : Observable<PokemonDetails> {
    return this.httpClient.get<PokemonDetails>(this.apiUrl + '/' + name)
  }
}

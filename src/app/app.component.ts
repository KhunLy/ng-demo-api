import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuItem, NbToastrService } from '@nebular/theme';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  items: NbMenuItem[] = [
    { title : 'Exercices', icon: 'bell-outline',  children: [
      { title: 'Exercice 1', link: '/exercice/ex1' },
      { title: 'Pokedex', link: '/exercice/pokedex' },
    ] }
  ];

  constructor(public authService: AuthService, private router: Router, private toastr: NbToastrService) {

  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/exercice/login');
    this.toastr.info('Au revoir');
  }
}

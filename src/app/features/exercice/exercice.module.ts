import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExerciceRoutingModule } from './exercice-routing.module';
import { ExerciceComponent } from './exercice.component';
import { Ex1Component } from './pages/ex1/ex1.component';
import { PokedexComponent } from './pages/pokedex/pokedex.component';
import { NbButtonModule, NbCardModule, NbDatepickerModule, NbInputModule, NbListModule, NbToastrModule } from '@nebular/theme';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ProfilComponent } from './pages/profil/profil.component';


@NgModule({
  declarations: [
    ExerciceComponent,
    Ex1Component,
    PokedexComponent,
    LoginComponent,
    ProfilComponent
  ],
  imports: [
    CommonModule,
    ExerciceRoutingModule,
    NbCardModule,
    NbListModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    ChartsModule,
    NbDatepickerModule.forRoot()
  ]
})
export class ExerciceModule { }

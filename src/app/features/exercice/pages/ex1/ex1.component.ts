import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/core/models/country';
import { CountryService } from 'src/app/core/services/country.service';

@Component({
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  pays!: Country[];
  isLoading = false;

  constructor(
    private countryService: CountryService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.countryService.getAll()
      .subscribe(data => {
        this.pays = data;
        this.isLoading = false;
      })
  }

}

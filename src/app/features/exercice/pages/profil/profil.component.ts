import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { User } from 'src/app/core/models/user';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserService } from 'src/app/core/services/user.service';
import { CustomValidators } from 'src/app/shared/validators/custom-validators';

@Component({
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  fg!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private toastr: NbToastrService,
  ) { }

  ngOnInit(): void {
    this.fg = this.fb.group({
      id: [],
      lastName: [null, [Validators.required, Validators.maxLength(50)]],
      firstName: [null, [Validators.required, Validators.maxLength(50)]],
      birthDate: [null, CustomValidators.NotAfter(new Date())]
    });
    this.fg.patchValue(<User>this.authService.user);
    this.fg.get('birthDate')?.setValue(new Date(<string>this.authService.user?.birthDate));
  }


  submit() {
    if(this.fg.invalid) return;
    this.userService.update(this.fg.value)
      .subscribe(() => {
        this.authService.refreshToken().subscribe();
        this.toastr.success('OK');
      }, error => {
        this.toastr.danger('KO');
      });
  }

}

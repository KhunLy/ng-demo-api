import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthService,
    private toastr: NbToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {

    // this.fg = new FormGroup({
    //   email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50)]),
    //   password: new FormControl(null, [Validators.required]),
    // });

    this.fg = this.fb.group({
      email: [null, [Validators.required, Validators.email, Validators.maxLength(50)]],
      password: [null, [Validators.required]]
    });
  }

  login() {
    if(this.fg.valid) {
      this.authServ.login(this.fg.value)
        .subscribe(() => {
          // en cas de success
          this.router.navigateByUrl('/exercice/ex1');
          this.toastr.success(`Bienvenue ${this.authServ.user?.name}`);
        }, error => {
          // en cas  d'erreur
          this.toastr.danger('la connection a échoué');
        });
      }
  }

}



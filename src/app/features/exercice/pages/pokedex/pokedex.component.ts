import { Component, OnInit } from '@angular/core';
import { Pokedex } from 'src/app/core/models/pokedex';
import { PokemonDetails } from 'src/app/core/models/pokemon-details';
import { PokedexService } from 'src/app/core/services/pokedex.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit {

  model!: Pokedex;
  selectedPokemon!: PokemonDetails;
  isLoading: boolean = false;
  limit: number = 50;
  offset: number = 0;

  public radarChartLabels: Label[] = [];

  public radarChartData: ChartDataSets[] = [];

  public radarChartOptions: ChartOptions = {
    scale: {
        ticks: {
            beginAtZero: true,
            max: 255,
            min: 0,
            stepSize: 50
        }
    }
  }

  constructor(
    private pokeService: PokedexService
  ) { }

  ngOnInit(): void {
    this.loadPokedex();
  }

  showDetails(name: string) {
    this.isLoading = true;
    this.pokeService.getDetails(name)
      .subscribe(data => {
        this.selectedPokemon = data;
        this.isLoading = false;
        this.setChart(data);
      });
  }

  setChart(data: PokemonDetails) {
    this.radarChartData.splice(0);
    this.radarChartData.push({
      label: data.name,
      data: data.stats.map(x => x.base_stat),
    });
    this.radarChartLabels = data.stats.map(x => x.stat.name);
  }

  next() {
    this.offset += this.limit;
    this.loadPokedex();
  }

  previous() {
    this.offset -= this.limit;
    this.loadPokedex();
  }

  private loadPokedex() {
    this.pokeService.get(this.limit, this.offset)
      .subscribe(data => this.model = data);
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DummyGuard } from 'src/app/core/guards/dummy.guard';
import { IsAuthenticatedGuard } from 'src/app/core/guards/is-authenticated.guard';
import { ExerciceComponent } from './exercice.component';
import { Ex1Component } from './pages/ex1/ex1.component';
import { LoginComponent } from './pages/login/login.component';
import { PokedexComponent } from './pages/pokedex/pokedex.component';
import { ProfilComponent } from './pages/profil/profil.component';

const routes: Routes = [
  { path: '', component: ExerciceComponent, children: [
    { path: 'ex1', component: Ex1Component },
    { path: 'pokedex', component: PokedexComponent },
    { path: 'login', component: LoginComponent, canDeactivate: [ DummyGuard ] },
    { path: 'profile', component: ProfilComponent, canActivate: [ IsAuthenticatedGuard ] },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExerciceRoutingModule { }

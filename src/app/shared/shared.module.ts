import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingButtonComponent } from './components/loading-button/loading-button.component';
import { LoadingComponent } from './components/loading/loading.component';
import { FormErrorComponent } from './components/form-error/form-error.component';



@NgModule({
  declarations: [
    LoadingButtonComponent,
    LoadingComponent,
    FormErrorComponent,
  ],
  imports: [
    CommonModule
  ],
  exports : [
    LoadingButtonComponent,
    LoadingComponent,
    FormErrorComponent,
  ]
})
export class SharedModule { }

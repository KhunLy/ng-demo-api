import { AbstractControl, ValidatorFn } from "@angular/forms";

export class CustomValidators {
    static NotAfter(date: Date) : ValidatorFn {
        return (control: AbstractControl) => {
            if(!control.value) return null;

            const dateToControl = <Date>control.value;

            if(dateToControl >= date) return { notBefore : true };

            // retourner null si pas d'erreur
            return null;
        }
    }
}